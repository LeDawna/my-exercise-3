﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myPeople = new List<Person>();

//      using (var db = new AdventureWorks2014Entities())
//      {
//        myPeople = db.People.Take(500).OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();

//      }
//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q1Path = @"../../query1.txt";
//      using (var swq1 = new StreamWriter(q1Path))
//      {
//        myPeople.ForEach(x => swq1.WriteLine("{0}, {1}", x.LastName, x.FirstName));
//      }
//    }
//  }
//}


          //  Query 1   //
//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {
//        myCustomers = db.Customers.OrderBy(x => x.AccountNumber).ToList();


//      }
//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q1Path = @"../../QueryOne.txt";
//      using (var swq1 = new StreamWriter(q1Path))
//      {
//        myCustomers.ForEach(x => swq1.WriteLine("{0}", x.AccountNumber));
//        swq1.WriteLine("My total customer count is {0}", myCustomers.Count);
//      }
//    }
//  }
//}
//End Query 1//

                                                       //Query 2//

//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {

//        myCustomers = db.Customers
//          .Include(x => x.Person)
//          .Include(x => x.Store)
//          .Include(x => x.SalesOrderHeaders)
//          .OrderBy(x => x.Person.LastName)
//          .ThenBy(x => x.Person.FirstName)
//          .ToList();
//      }



//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q2Path = @"../../QueryTwo.txt";
//      using (var swq2 = new StreamWriter(q2Path))
//      {
//        //customerDetails.ForEach(x => swq2.WriteLine("{0}", x));

//        foreach (var p in myCustomers)
//        {
//          swq2.WriteLine("{0}, {1}, {2}, {3}, {4}",
//            p.CustomerID,
//            (p.Person != null) ? p.Person.LastName : "N/A",
//            (p.Person != null) ? p.Person.FirstName : "",
//            p.AccountNumber,
//            (p.Store != null) ? p.Store.Name : "N/A"
//            );

//              foreach (var item in p.SalesOrderHeaders)
//              {
//               swq2.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax {3:C}",
//                 item.SubTotal,
//                 item.Freight,
//                 item.TotalDue,
//                 item.TaxAmt
//                 );
//              }
//        }
//        swq2.WriteLine("There are {0} customers", myCustomers.Count);

//      }
//    }

//  }

                        //End Query 2 //


                        //Query 3//
//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {

//        myCustomers = db.Customers
//          .Include(x => x.Person)
//          .Include(x => x.Store)
//          .Include(x => x.SalesOrderHeaders)
//          .Where(x => x.Person.LastName == "Chapman")
//          .OrderBy(x => x.Person.LastName)
//          .ThenBy(x => x.Person.FirstName)
//          .ToList();
//      }
//      var q3Path = @"../../QueryThree.txt";
//      using (var swq2 = new StreamWriter(q3Path))
//      {
//        foreach (var p in myCustomers)
//        {
//          swq2.WriteLine("{0}, {1}, {2}, {3}, {4}",
//            p.CustomerID,
//            (p.Person != null) ? p.Person.LastName : "N/A",
//            (p.Person != null) ? p.Person.FirstName : "",
//            p.AccountNumber,
//            (p.Store != null) ? p.Store.Name : "N/A"
//            );

//          foreach (var item in p.SalesOrderHeaders)
//          {
//            swq2.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax {3:C}",
//              item.SubTotal,
//              item.Freight,
//              item.TotalDue,
//              item.TaxAmt
//                           );
//          }
//        }
//        swq2.WriteLine("There are {0} customers", myCustomers.Count);

//      }
//    }
//  }
//}

                                     //End Query 3  //

                        //Query 4 -incomplete//
{
  class Program
  {
    static void Main(string[] args)
    {
      var myCustomers = new List<Customer>();

      using (var db = new AdventureWorks2014Entities())
      {

        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ToList();
      }
      var q2Path = @"../../QueryTwo.txt";
      using (var swq2 = new StreamWriter(q2Path))
      {
        foreach (var p in myCustomers)
        {
          swq2.WriteLine("{0}, {1}, {2}, {3}, {4}",
            p.CustomerID,
            (p.Person != null) ? p.Person.LastName : "N/A",
            (p.Person != null) ? p.Person.FirstName : "",
            p.AccountNumber,
            (p.Store != null) ? p.Store.Name : "N/A"
            );

          foreach (var item in p.SalesOrderHeaders)
          {
            swq2.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax {3:C}",
              item.SubTotal,
              item.Freight,
              item.TotalDue,
              item.TaxAmt
              );
          }
        }
        swq2.WriteLine("There are {0} customers", myCustomers.Count);

      }
    }
  }
}
//End Query 4 //


